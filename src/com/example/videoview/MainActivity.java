package com.example.videoview;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class MainActivity extends Activity implements OnTouchListener {

	private VideoView myVideoView;
	private MediaController mediaControls;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Crea els botons per controlar el vídeo
		if (mediaControls == null) {
			mediaControls = new MediaController(MainActivity.this);
		}

		myVideoView = (VideoView) findViewById(R.id.video_view);

		try {
			// Afegeix els controls
			myVideoView.setMediaController(mediaControls);

			// Indica el vídeo per reproduir
			myVideoView.setVideoURI(Uri.parse("android.resource://"
					+ getPackageName() + "/" + R.raw.argo));

		} catch (Exception e) {
			Log.e("Error", e.getMessage());
			e.printStackTrace();
		}

		myVideoView.setOnTouchListener(this);
	}

	// Detecta quan es prem sobre la pantalla
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (myVideoView.getCurrentPosition() == 0) {
			myVideoView.start();
			Toast.makeText(getApplicationContext(),
					"És la 1a vegada que premo", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(getApplicationContext(),
					"Ara no és la 1a vegada que premo", Toast.LENGTH_SHORT)
					.show();
		}

		return false;
	}
}
